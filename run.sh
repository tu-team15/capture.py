#!/usr/bin/env sh

for i in "$@";
do
    params=" $params $i"
done

python3 -m capture $params
