#!/usr/bin/env python3

from argparse import ArgumentParser

from .capture import Capture

commands = {
    "http": "wget -p -e robots=off --limit-rate={rate} --no-check-certificate"
    " --delete-after {path}",
    "ftp": "wget --limit-rate={rate} --user=[username] --password=[password]"
    " --no-check-certificate --delete-after {path}",
    "lftp": "lftp -c \"set ssl:verify-certificate false && set net:limit-rate"
    " {rate} && open -u [username],[password] 127.0.0.1 && get -e {path}\"",
    "ssh": "rsync --bwlimit={rate} --ignore-times --whole-file {path}"
    " .background.tmp"
}


def main():
    parser = ArgumentParser(
        description="capture.py is a tool used to measure network traffic."
    )
    parser.add_argument("method", type=str, help="Method of transfer",
                        choices=commands.keys())
    parser.add_argument("-n", "--samples", dest="samples", type=int,
                        default=100, help="Number of captures.")
    parser.add_argument("-t", "--target", dest="target", type=str,
                        required=True,
                        help="Location to measure traffic from.")
    parser.add_argument("-i", "--interface", dest="interface", type=str,
                        default="lo", help="Interface to monitor traffic on.")
    parser.add_argument("-r", "--rate", dest="rate", type=str,
                        default="0", help="Rate limit.")
    parser.add_argument("--sloppy", action="store_false",
                        help="Process captures with packet loss.")
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="Print debug information.")
    args = parser.parse_args()

    command = commands[args.method].format(path=args.target, rate=args.rate)

    c = Capture(samples=args.samples,
                command=command,
                interface=args.interface,
                strict=args.sloppy,
                verbose=args.verbose)
    c.capture()


if __name__ == "__main__":
    main()
