#!/usr/bin/env python3

import sys
import re
import logging
from json import dump

import signal
import subprocess
import shlex
import time

CMD_TSHARK = "tshark -B 100 -V -i {interface}"
BYTE_PATTERN = re.compile(r"(\d+) bytes on wire")
LOSS_PATTERN = re.compile(r"(\d+) packets dropped")

FILENAME = "capture"
LOG_FILE = FILENAME + ".log"
JSON_FILE = FILENAME + ".json"

logger = logging.getLogger(__name__)


class Capture():
    def __init__(self, samples, command, interface, strict=True, verbose=True):
        if type(interface) is not str:
            raise TypeError("Interface has to be of type str.")
        if type(samples) is not int:
            raise TypeError("Samples has to be of type int.")
        if type(strict) is not bool:
            raise TypeError("Strict has to be of type bool.")
        if samples <= 0:
            raise ValueError("Cannot capture less than one sample")

        self.samples = samples
        self.command = command
        self.interface = interface
        self.strict = strict
        self.verbose = verbose

        if verbose:
            level = logging.DEBUG
        else:
            level = logging.INFO

        logger.setLevel(level)

        fmt = "%(asctime)s [%(levelname)s] %(message)s"
        datefmt = "%Y-%m-%d %H:%M:%S"
        formatter = logging.Formatter(fmt=fmt, datefmt=datefmt)

        sh = logging.StreamHandler(sys.stdout)
        sh.setFormatter(formatter)
        logger.addHandler(sh)

        fh = logging.FileHandler(LOG_FILE, "w")
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    def capture(self):
        logger.info("Start capturing.")
        logger.debug("Command: " + self.command)

        captures = []
        n = 0

        while n < self.samples:
            msg = "Capturing sample {} of {}."
            logger.info(msg.format(n + 1, self.samples))

            try:
                stdout, stderr = self._capture()
                logger.debug("Processing capture.")
                packet_sum = self._process_capture(stdout, stderr)
                captures.append(packet_sum)
                n += 1
            except CaptureException as e:
                msg = "Caught exception while capturing: {}"
                logger.warning(msg.format(e.msg))

                if not self.strict:
                    n += 1

        logger.info("Finished capturing.")

        capture_count = len(captures)
        capture_sum = sum(captures)
        capture_mean = capture_sum / float(max(capture_count, 1))

        msg = "capture count: {}, capture sum: {}, capture mean: {}"
        logger.info(msg.format(len(captures), capture_sum, capture_mean))

        with open(JSON_FILE, "w") as json_file:
            dump(captures, json_file, indent="\t")

    def _process_capture(self, stdout, stderr):
        loss = re.search(LOSS_PATTERN, stderr)
        if self.strict and loss is not None:
            loss_count = loss.group(1)
            raise CaptureException("Capure has packet loss: " + loss_count)

        byte_counts = []
        iterator = re.finditer(BYTE_PATTERN, stdout)

        for match in iterator:
            extract = match.group(1)
            try:
                byte_count = int(extract)
                byte_counts.append(byte_count)
            except ValueError:
                msg = "Could not interpret {} as integer."
                logger.warning(msg.format(extract))

        packet_count = len(byte_counts)
        packet_sum = sum(byte_counts)
        packet_mean = packet_sum / float(max(packet_count, 1))

        msg = "packet count: {}, packet sum: {}, packet mean: {}"
        logger.debug(msg.format(packet_count, packet_sum, packet_mean))

        return packet_sum

    def _capture(self):
        logger.debug("Starting tshark capture.")
        cmd = CMD_TSHARK.format(interface=self.interface)
        args = shlex.split(cmd)
        p0 = subprocess.Popen(args,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE)

        time.sleep(1)

        logger.debug("Starting transfer.")
        args = shlex.split(self.command)
        p1 = subprocess.Popen(args, stderr=subprocess.DEVNULL)

        try:
            p1.wait(timeout=60)
        except subprocess.TimeoutExpired:
            raise CaptureException("Timeout expired")

        time.sleep(1)

        logger.debug("Terminating tshark.")
        p0.send_signal(signal.SIGINT)

        stdout, stderr = p0.communicate()
        stdout = stdout.decode(encoding="UTF-8")
        stderr = stderr.decode(encoding="UTF-8")

        return stdout, stderr


class CaptureException(Exception):
    def __init__(self, msg):
        self.msg = msg
